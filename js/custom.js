/**
 * @file
 * JavaScript for notification.
 */

(function ($) {
  function notification() {
	  
	// var x = $(".block-follower-notifications-block").position(); 
	  //console.log(x.left);
    $('.block-follower-notifications-block .notification-wrapper a').on("click",function (e) {
		/*alert("test");*/
		
		
		$('.block-follower-notifications-block .outer-wrapper').addClass("show-notification");
		var notificationCount = $('.block-follower-notifications-block .outer-wrapper>li').length;
		var notificationBlockHeight = 0;
		if(notificationCount > 10){
			$('.block-follower-notifications-block .outer-wrapper>li').each(function(){
				notificationBlockHeight = notificationBlockHeight + $(this).height();
			});
			console.log(notificationBlockHeight);
			$('.block-follower-notifications-block .outer-wrapper').css("height",notificationBlockHeight);
			$('.block-follower-notifications-block .outer-wrapper').css("overflow-y","scroll");
		}		
    });
    $('.block-follower-notifications-block .notification-wrapper .close-btn').on("click",function () {
		$('.block-follower-notifications-block .outer-wrapper').removeClass("show-notification");
    });

	
  }
  $(document).ready(function () {
    notification();
  });
}(jQuery));
