<?php

namespace Drupal\follower_notifications\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Follower notification settings form.
 */
class FollowerNotificationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'follower_notifications.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'follower_notificationsettings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $node_types = NodeType::loadMultiple();
    $values = [];
    $comment_values = [];
    foreach ($node_types as $node_type) {
      $values[$node_type->id()] = $node_type->label();
    }
    $comment_values['mail'] = 'Notify through mail.';
    $comment_values['bell'] = 'Bell notification';
    $config = $this->config('follower_notifications.adminsettings');

    $form['new_option_options'] = [
      '#type' => 'checkboxes',
      '#title' => t('Select the Content types for which the notification need to be applied.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 10,
      '#options' => $values,
      '#default_value' => $config->get('new_option_options'),

    ];
    $form['comment_notify'] = [
      '#type' => 'checkboxes',
      '#title' => t('Notify the content authors when new comment inserted.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 10,
      '#options' => $comment_values,
      '#default_value' => $config->get('comment_notify'),

    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    print_r($form_state->getValue('new_option_options'));
    print_r($form_state->getValue('comment_notify'));
    $this->config('follower_notifications.adminsettings')
      ->set('new_option_options', $form_state->getValue('new_option_options'))
      ->set('comment_notify', $form_state->getValue('comment_notify'))
      ->save();
  }

}
