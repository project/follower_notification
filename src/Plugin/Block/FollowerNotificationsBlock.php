<?php

namespace Drupal\follower_notifications\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with list of notification items.
 *
 * @Block(
 *   id = "follower_notifications_block",
 *   admin_label = @Translation("Follower Notifications block"),
 *   category = @Translation("Follower Notifications Module")
 * )
 */
class FollowerNotificationsBlock extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * Drupal\Core\Session\AccountInterface definition.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Database Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountInterface $current_user,
    Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->database    = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Save our custom settings when the form is submitted.
    $this->setConfigurationValue('', $form_state->getValue(''));
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIf($account->isAuthenticated());
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $connection = $this->database;
    $config     = $this->getConfiguration();

    // Get logged user session.
    $currentUser = $this->currentUser;
    $uid = $currentUser->id();
    $notificationList = [];
    $message = [];
    $getNotification = $connection->select('notifications', 'n');
    $getNotification->fields('n', ['entity_id', 'entity_uid']);
    $getNotification->fields('n', ['message', 'action']);
    $getNotification->condition('n.status', 1, '=');
    $getNotification->condition('n.uid', $uid, '=');
    $getNotification->orderBy('n.created', 'DESC');
    $results = $getNotification->execute()->fetchAll();
    $notificationList['new-notifications'] = count($results);
    global $base_url;
    foreach ($results as $val) {
      $notificationList['node_id'] = $val->entity_id;
      $notificationList['message'] = $val->message;
      $notificationList['action'] = $val->action;
      if ($notificationList['action'] == "Create") {
        $message[] = [
          'url' => $base_url . '/node/' . $notificationList['node_id'],
          'text' => 'New post has been created ' . '"' . $notificationList['message'] . '"',
        ];
      }
      else {
        $message[] = [
          'url' => $base_url . '/node/' . $notificationList['node_id'],
          'text' => 'New comment posted for the title' . '"' . $notificationList['message'] . '"',
        ];
      }
    }
    return [
      '#theme' => 'follower_notifications',
      '#notificationcount' => $notificationList['new-notifications'],
      '#messagebody' => $message,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
